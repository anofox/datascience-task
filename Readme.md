# Applicant Task: Data Science and Analytics for Enterprise Forecasting

## What to do?

We have some simple questions according to statistics & machine learning. Please prepare these questions for the second interview round (in any form you want). Therefore, we added two example time series and one time series set in the `data` directory:

1. `series_example_1.parquet`

2. `series_example_2.parquet`

3. `time_series_set.parquet`


### Task 1: Single Series

1. May you plot the time series `series_example_1.parquet` and `series_example_2.parquet` with Python in a Jupyter notebook?

2. How do you interpret the time series? Which industry do you expect for series 1 and which for series 2? Explain your thoughts!

3. Both series have missing values. How would you interpret and impute these missing values? Why?


### Task 2: Multiple Series

Let's have a look at `time_series_set.parquet`. 

1. How many time series are in this set?

2. What would your strategy for getting insights in such a "big" time series set?

3. How many different "types" of series does this set have? Please support your answer by an analyis in a Jupyter notebook.

4. We used the long format for our time series set. A wide format would be also possible. What are the advantages of these two types of time series representation?


### Task 3: "Optimal" Point Forecast

Loss functions measure accuracy/performance and define "optimal" point forecasts. RMSE, MAPE, and MAD are some standard metrics used in forecasting.

1. Could you explain advantages / disadvantages of these three metrics?

2. Assume you have 100 iid samples from a Gamma distribution with shape and scale 3. What is the "optimal" forecast for each of these error metrics? Make an example Python.
